document.getElementById('timer-setup').addEventListener('input', e => {
		let t = e.target;
		if (+t.value < 10) {
			t.style.borderColor = 'red';
		} else { 
			t.style.borderColor = 'inherit';
			chrome.runtime.sendMessage({timeout: +t.value});
		}
});
