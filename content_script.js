let cinv;

function autoSave(time) {
	if (!time) return;
	if (cinv) clearInterval(cinv);
	cinv = setInterval(() => {const btn = document.querySelector('[ng-click="savePlan()"]'); if (btn) btn.click()}, +time * 1000);
}

autoSave(60);
chrome.runtime.sendMessage('showPageAction')
