chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if (msg == 'showPageAction') chrome.pageAction.show(sender.tab.id);
	else if (typeof msg == 'object' && msg.timeout) {
		chrome.tabs.getSelected(tab => chrome.tabs.executeScript(tab.id, {'code':`autoSave(${msg.timeout})`}));
	}
});